console.log("Hello World")

//JAVASCRIPT SYNCHRONOUS AND ASYNCHRONOUS
/*
	SYNCHRONOUS
		Synchronous codes runs in sequence. This means that each operaion must wait for the previous one to complete before executing
	ASYNCHRONOUS
		Asynchronous codes runs in parallel. This means that an operation can occur while another one is still being processed

		Asynchronous code execution is often preferrable in situations where execution can be blocked. Some examples of this area network requests, long-running calculations, file system operations, etc. Using asynchronous code in the browser ensures the page remains response and the user experience is mostly unaffected
*/

/*Example: concept of synchronous, it will not execute if the other part of the code has an error

	console.log("Hello World")
	cosnole.log("Hello World") //it will have an error
	for (let i = 0; i <= 10000; i++){
		console.log(i) //for this line it will load first after the for loop finish the Hello it's me will load
	}
	console.log("Hello it's me")
*/

/*
	API stands for application programming interface
		- An application programming interface is a particular set of codes that allows software programs to communicate with each other
		- An API is the interface through which you access someone else's code or through which someone else's code accesses yours
		
		Example:
			Youtube API
			-https://developers.google.com/youtube/iframe_api_reference
			Google API
			-https://developers.google.com/identity/sign-in/web/sign-in
*/

//FETCH API

	/*console.log(fetch('https://jsonplaceholder.typicode.com/posts'))*/

/*
	- A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

	A promise is in one of these 3 stages
	PENDING:
		- Initial state, neither fulfilled nor rejected
	FULFILLED:
		- Operation was successfully completed
	REJECTED: 	
		- Operation failed
*/

	/*
	By using the .then method, we can now check for the status of the promise 
	The 'fetch' method will return a 'promise' that resolves to be a 'response' object
	The '.then' method captures the 'response' objects and returns another 'promise' which will eventually be 'resolved' or 'rejected'
	Syntax:
		fetch('url')
		.then((response) => {})
	*/
	/*fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => console.log(response.status)) //result 200 means ok that will show on the console

	fetch('https://jsonplaceholder.typicode.com/posts')
	//use the 'json' method from the 'response' object to convert the data retrieved into JSON format to be used in our application
	.then(response => response.json())
	//Print the converted JSON value from the 'fetch' request
	//Using multiple '.then' method to create a promise chain
	.then(json => console.log(json)) */

/*
	- The 'async' and "await" keywords is another approach that can be used to achieve asynchronous codes
	- Used in functions to indicate which portions of code should be waited
	- Created a asynchronous function
*/
	
	/*async function fetchData () {
		//waits for the 'fetch' method to complete then stores the value in the 'result' variable
		let result = await fetch ('https://jsonplaceholder.typicode.com/posts')
		//Result returned by fetch is returned as a promise
		console.log(result);
		//the returned 'response' in an object
		console.log(typeof result);
		//we cannot access the content of the 'response' by directly accessing it's body property
		console.log(result.body)
		let json = await result.json();
		console.log(json)
			};

			fetchData();*/

//GETTING A SPECIFIC POST
//Retrieves a specific post following the REST API (retrieve, /post/id:, GET)

	/*fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response) => response.json())
	.then((json) => console.log(json));
*/
/*
	POSTMAN

		url: https://jsonplaceholder.typicode.com/posts/1
		method: GET
*/

/*
	CREATING A POST
	Syntax:
		fetch('URL', options)
		.then(() => {})
		.then((response) => {})
*/

//CREATES A NEW POST following the REST API (create, /post/:id, POST)
	/*fetch('https://jsonplaceholder.typicode.com/posts', {
		method: "POST",
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'New post',
			body: 'Hello World',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))
*/
/*
	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts
	method: POST
	body: raw + json
		{
			"title": "My First Blog Post",
			"body": "Hello world!",
			"userId": 1
		}
*/

//UPDATING A POST
//updates a specific post following the REST API (update, /posts/:id, PUT)
	/*fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			id: 1,
			title: 'Updated post',
			body: 'Hello again!',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))
*/
/*
	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts
	method: PUT
	body: raw + json
		{
			"title": "My First Revised Blog Post",
			"body": "Hello there! I revised this a bit.",
			"userId": 1
		}
*/

//Update a post using the PATCH method
/*
	- updates a specific post following the REST API(update, /posts/:id, PATCH) 
	The differences between PUT and PATCH is the number of properties being changed
	PATCH updates the part
	PUT updates the whole document
*/
	/*fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Corrected post'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))
*/
/*
	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts
	method: PATCH
	body: raw + json
		{
			"title": "This is my final title"
		}


/*DELETING A POST
Deleting a specific post following the REST API (update, /posts/:id, DELETE) */

/*fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE',
})

	POSTMAN
	url: https://jsonplaceholder.typicode.com/posts
	method: DELETE*/

//Filetering the post
/*
	The data can be filtered by sending the userId along with the URL
	Information sent via the URL can be done by adding the question mark symbol(?)
	Syntax:
		Individual Parameters:
			'url?parameterName=value'
		Multiple Parameters:
			'url?ParamA=valueA&paramB=valueB'
*/
	/*fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
	.then((response) => response.json())
	.then((json) => console.log(json));*/

	/*fetch('https://jsonplaceholder.typicode.com/posts?userId=1&userId=2&userId=3')
	.then((response) => response.json())
	.then((json) => console.log(json));*/

//RETRIEVE comments of a specific post
//retrieving comments for a specific post following the REST API (retrieve, /posts/id:, GET)

	/*fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
	.then((response) => response.json())
	.then((json) => console.log(json));*/

	fetch('http://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Created To Do List Item',
		/*	completed: false,*/
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))