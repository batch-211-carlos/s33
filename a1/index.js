fetch('http://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

//Getting all TO DO LIST ITEM
fetch('http://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);
}

//Getting a specific todo list item

fetch('http://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

//Create a to do list item using post method
	fetch('http://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Created To Do List Item',
			completed: false,
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//UPDATE to do list item using PUT METHOD
fetch('http://jsonplaceholder.typicode.com/todos', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Created To Do List Item',
			description: 'To update the my to do list with a different data structure.',
			status: 'Pending',
			dataCompleted: 'Pending',
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//Updating a to do list item using the Patch Method

fetch('http://jsonplaceholder.typicode.com/todos', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			status: 'completed',
			dataCompleted: '07/09/21'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))


//Delete a to do list item
	fetch('http://jsonplaceholder.typicode.com/todos',{
	method: 'DELETE'